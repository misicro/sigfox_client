package com.intel.sigfoxclientapp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.os.Handler;

import java.util.Timer;
import java.util.TimerTask;

public class GPSTracker {
    final static long gpsRefreshRate = 4;
    static private final Boolean LOGS = true;
    private static final String TAG = "GPS_Tracker";
    private static final int switchToCellPositionThreshold = 2;
    static long defaultMinTime = 1000 * 20; //20 seconds interval min to search for distance changes
    static long mLastLocationMillis = 0;
    // The minimum distance to change Updates in meters
    private static long defaultMinDistance = 100 ;  //default 100 meters between updates
    private static int counter = 0;
    private final Context mContext;
    // Declaring a Location Manager
    protected LocationManager GPSLocationManager, CellLocationManager;
    // Flag for GPS status
    boolean isGPSEnabled = false;
    // Flag for network status
    boolean isNetworkEnabled = false;
    // Flag for GPS status
    boolean canGetLocation = false;
    boolean firstTimeLocation = false;
    Location location; // Location
    double latitude, longitude;
    Boolean isGPSData = false, isTimerOngoing = false, isNetworkData = false;
    SigfoxBackgroundService mSigfoxBackgroundService;
    LocationListener NetworkLocationListener;
    LocationListener GPSLocationListener;
    Location gps_location, cell_location;
    Timer timer;
    TimerTask AutoUpdateTimerTask;

    public GPSTracker(Context context, SigfoxBackgroundService service) {
        this.mSigfoxBackgroundService = service;
        this.mContext = context;

        GPSLocationManager =(LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        CellLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        NetworkLocationListener = new MyNetworkLocationListener();
        GPSLocationListener = new MyGPSLocationListener();
        counter = 0;
    }

    public Boolean requestLocationUpdatesOverNetwork(long minTimeBetweenUpdates, long minDistanceChangeForUpdates) {
        try {
            if (CellLocationManager != null) {
                if (ContextCompat.checkSelfPermission(this.mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(this.mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    isNetworkEnabled = CellLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                    if (isNetworkEnabled) {
                        if(minDistanceChangeForUpdates == 0) minDistanceChangeForUpdates = defaultMinDistance;
                        CellLocationManager.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER,
                                minTimeBetweenUpdates / gpsRefreshRate,
                                minDistanceChangeForUpdates, NetworkLocationListener);
                        if(LOGS) L.msg(TAG, "requestLocationUpdates NETWORK_PROVIDER time between updates / dst : " + minTimeBetweenUpdates / (gpsRefreshRate * 1000) + " sec /" +
                                minDistanceChangeForUpdates + " meters");
                        if(!isTimerOngoing) {
                            isTimerOngoing = true;
                            if(minTimeBetweenUpdates == 0) minTimeBetweenUpdates = defaultMinTime; //make sure we have the timer working
                            timer = new Timer();
                            AutoUpdateTimerTask = new timeoutCheckForPosition();
                            timer.scheduleAtFixedRate(AutoUpdateTimerTask, minTimeBetweenUpdates, minTimeBetweenUpdates);
                        }
                        return true;
                    }
                    return false;
                }
            } else {
                if(LOGS) L.msg(TAG, "locationManager is null. Possible GPS stack not running");
                return false;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Boolean requestLocationUpdatesOverGPS(long minTimeBetweenUpdates, long minDistanceChangeForUpdates) {
        try {
            if (GPSLocationManager != null) {
                if (ContextCompat.checkSelfPermission(this.mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(this.mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    isGPSEnabled = GPSLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    if (isGPSEnabled) {
                        if(minDistanceChangeForUpdates == 0) minDistanceChangeForUpdates = defaultMinDistance;
                        GPSLocationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                minTimeBetweenUpdates / gpsRefreshRate,
                                minDistanceChangeForUpdates, GPSLocationListener);
                        if(LOGS) L.msg(TAG, "requestLocationUpdates GPS_PROVIDER time between updates / dst : " + minTimeBetweenUpdates / (gpsRefreshRate * 1000) + " sec /" +
                                minDistanceChangeForUpdates + " meters");
                        if (!isTimerOngoing) {
                            isTimerOngoing = true;
                            if(minTimeBetweenUpdates == 0) minTimeBetweenUpdates = defaultMinTime;  //make sure we have the timer working
                            timer = new Timer();
                            AutoUpdateTimerTask = new timeoutCheckForPosition();
                            timer.scheduleAtFixedRate(AutoUpdateTimerTask, minTimeBetweenUpdates, minTimeBetweenUpdates);
                        }
                        return true;
                    }
                    return false;
                }
            } else {
                if(LOGS) L.msg(TAG, "locationManager is null. Possible GPS stack not running");
                return false;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Location getLocation() {
        try {
            if (GPSLocationManager != null && CellLocationManager != null) {
                if (ContextCompat.checkSelfPermission(this.mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(this.mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    isGPSEnabled = GPSLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    isNetworkEnabled = CellLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                    if (isGPSEnabled && isNetworkEnabled) {
                        this.canGetLocation = true;
                        location = GPSLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                            isGPSData = true;
                            isNetworkData = false;
                        } else {
                            location = CellLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                                isGPSData = false;
                                isNetworkData = true;
                            }
                        }
                    }
                }
            } else {
                if(LOGS) L.msg(TAG, "locationManager is null. Possible GPS stack not running");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS
     * */
    public void stopGPSautoUpdates(){
        if(timer != null) {
            AutoUpdateTimerTask.cancel();
            timer.cancel();
            timer.purge();
            isTimerOngoing = false;
            firstTimeLocation = false;
            if(LOGS) L.msg(TAG, "AutoUpdate Timer canceled");
        }
        if (GPSLocationManager != null) {
            if (ContextCompat.checkSelfPermission(this.mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this.mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                GPSLocationManager.removeUpdates(GPSLocationListener);
                if(LOGS) L.msg(TAG, "GPS UPDATES removed");
            }
        }
        if (CellLocationManager != null) {
            if (ContextCompat.checkSelfPermission(this.mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this.mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                CellLocationManager.removeUpdates(NetworkLocationListener);
                if(LOGS) L.msg(TAG, "CELL UPDATES removed");
            }
        }
    }

    /**
     * Function to get latitude
     * */
    public double getLatitude(){
        if(location != null){
            latitude = location.getLatitude();
        }
        return latitude;
    }

    /**
     * Function to get longitude
     * */
    public double getLongitude(){
        if(location != null){
            longitude = location.getLongitude();
        }
        return longitude;
    }

    /**
     * Function to check GPS/Wi-Fi enabled
     * @return boolean
     * */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }
    /**
     * Function to check if we have info from network or from GPS sensor
     * @return boolean
     * */

    public boolean isGPSBased() {
        return this.isGPSData;
    }

    private void chooseBestPosition() {

        /*We prefer GPS position over NETWORK one. So, we always wait for GPS data.
        * If GPS data is not present (switchToCellPositionThreshold) times, then we read once
        * the last known position from NETWORK (we can suppose that subject entered a building
        * and no more GPS data).Soon as we have GPS data, we will restart above algo*/

        if(gps_location !=  null) {
            if(gps_location.getAccuracy() <= 50) {
                mSigfoxBackgroundService.LocationUpdateCallback(gps_location);
                gps_location = null;
                counter = 0;
                if (LOGS) L.msg(TAG, "AutoUpdate location --> GPS");
            }
        } else {
            counter++;
        }
        if(counter == switchToCellPositionThreshold) {
            if (ContextCompat.checkSelfPermission(this.mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this.mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                //send once NETWORK position
                mSigfoxBackgroundService.LocationUpdateCallback(CellLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));
            }
            if(LOGS) L.msg(TAG, "AutoUpdate location --> NETWORK");
        } else if(LOGS) L.msg(TAG, "Waiting for valid position from GPS. AutoUpdate in progress");
    }

    private class MyNetworkLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            cell_location = location;
            if(LOGS) L.msg(TAG, "onLocationChanged >>>>>> NETWORK");
            /*if(!firstTimeLocation) {
                chooseBestPosition();
                firstTimeLocation = true;
                if(LOGS) L.msg(TAG, "FIRST LOCATIO FROM NETWORK");
            }*/
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {
        }

        @Override
        public void onProviderDisabled(String s) {
        }

    }

    private class MyGPSLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            if (location == null) return;
         //   mLastLocationMillis = SystemClock.elapsedRealtime();
            gps_location = location;
            if(LOGS) L.msg(TAG, "onLocationChanged >>>>>> GPS");

        //    mLastLocation = location;

          /*if(!firstTimeLocation) {
              chooseBestPosition();
              firstTimeLocation = true;
              if(LOGS) L.msg(TAG, "FIRST LOCATION FROM GPS");
          }*/
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
            int sat = -1;

            if (bundle.containsKey("satellites")) {
                 sat = bundle.getInt("satellites");
            }
            if (LOGS) L.msg(TAG, "NUMBER OF   SATELITES: " + sat);
            /*switch (i) {
                case GpsStatus.GPS_EVENT_STARTED:
                  //  if (LOGS) L.msg(TAG, "onStatusChanged>>>   GPS_EVENT_STARTED");
                    break;
                case GpsStatus.GPS_EVENT_STOPPED:
                  //  if (LOGS) L.msg(TAG, "onStatusChanged>>>   GPS_EVENT_STOPPED");
                    break;
                case GpsStatus.GPS_EVENT_FIRST_FIX:
                    if (LOGS) L.msg(TAG, "onStatusChanged>>>   GPS_EVENT_FIRST_FIX");
                   // isGPSFix = true;
                    break;
                case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                    if (LOGS) L.msg(TAG, "onStatusChanged>>>   GPS_EVENT_SATELLITE_STATUS");
                   *//* if (mLastLocation != null)
                        isGPSFix = (SystemClock.elapsedRealtime() - mLastLocationMillis) < 3000;
*//*
                    *//*if (isGPSFix) { // A fix has been acquired.
                        if (LOGS) L.msg(TAG, "!!!!!!We have a GPS fix");
                    } else { // The fix has been lost.
                        if (LOGS) L.msg(TAG, "GPS FIX has been lost!!!!!");
                    }*//*
                    break;
            }*/
        }

        @Override
        public void onProviderEnabled(String s) {
            if (LOGS) L.msg(TAG, "GPS ON --------------------------");
        }

        @Override
        public void onProviderDisabled(String s) {
            if (LOGS) L.msg(TAG, "GPS was turned OFF. Stopping Service");
            Intent i = new Intent(MainActivity.RECEIVE_BROADCAST);
            Bundle extras = new Bundle();
            extras.putString("command", "stop");
            i.putExtras(extras);
            mContext.sendBroadcast(i);
        }
    }

    public class timeoutCheckForPosition extends TimerTask {
        private Handler mHandler = new Handler();

        @Override
        public void run() {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            chooseBestPosition();
                        }
                    });
                }
            }).start();
        }
    }
}
