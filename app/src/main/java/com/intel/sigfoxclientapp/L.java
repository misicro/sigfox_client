package com.intel.sigfoxclientapp;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * helping class for logging
 */
public class L {
    public static void msg(String tag, String  message) {
        Log.d(tag, message);
    }
    public static void toast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

}

