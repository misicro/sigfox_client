package com.intel.sigfoxclientapp;

import android.app.ActivityManager;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import mehdi.sakout.fancybuttons.FancyButton;

public class MainActivity extends AppCompatActivity {
    public static final String RECEIVE_BROADCAST = "com.intel.sigfoxclientapp.RECEIVE_UNDER_ACTIVITY";
    private static final String TAG = "SgfClientActivity";
    private static final String keyNameTAG = "SIGFOX";
    private static final int DEFAULT_TIMEOUT_COUNT = 3;
    private static final int DEFAULT_TIMEOUT = 10;
    private static final int DEFAULT_GPS_DISTANCE = 50;
    private static final int DEFAULT_GPS_TIME = 15;
    private final int REQUEST_ENABLE_BT = 1;
    private final int MIN_gpsTime = 5;  //sec
    private final int MIN_gpsDistance = 0;  //m
    private final int MIN_timeoutCount = 1;  //times

    static MainActivity mainActivityReference;
    private static String rec_data = "null";
    final Messenger mMessenger = new Messenger(new IncomingHandler());
    private TextView mTimeoutVal;
    private TextView mTimeoutCountVal;
    private TextView mGpsTimeVal;
    private TextView mGpsDistanceVal;
    private TextView mStatusText;
    private SeekBar mTimeout, mTimeoutCount, mGpsDistance, mGpsTime;
    private FancyButton mButtonStartStop, mButtonReset, mBbuttonOpenBT, mButtonBTDiscovery;
    private Boolean toggle_pressed_state = false;
    private BluetoothAdapter mBtAdapter;
    private Messenger mService = null;
    private boolean mIsBound;
    private LocationManager locationManager;
    private boolean isGPSEnabled = false, isNetworkEnabled = false;
    private ServiceConnection mCon = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = new Messenger(service);
            try {
                Message msg = Message.obtain(null, SigfoxBackgroundService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);
            } catch (RemoteException e) {
                // In this case the service has crashed before we could even do anything with it
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            mService = null;
        }
    };
    //BD receiver for stoping the service from other classes
    private BroadcastReceiver myReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent.getStringExtra("command") != null) {
                rec_data = intent.getStringExtra("command");
                if (rec_data.equals("stop")) {
                    stopService();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firstTimeApp();

        mStatusText = (TextView) findViewById(R.id.status_text);
        Typeface typefaceRunningStatus = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Oswald-ExtraLight.ttf");
        mStatusText.setTypeface(typefaceRunningStatus);

        mTimeout = (SeekBar) findViewById(R.id.timeout);
        mTimeoutVal = (TextView) findViewById(R.id.timeout_val);

        mTimeoutCount = (SeekBar) findViewById(R.id.timeout_count);
        mTimeoutCountVal = (TextView) findViewById(R.id.timeout_count_val);

        mGpsTime = (SeekBar) findViewById(R.id.gps_time);
        mGpsTimeVal = (TextView) findViewById(R.id.gps_time_val);

        mGpsDistance = (SeekBar) findViewById(R.id.gps_distance);
        mGpsDistanceVal = (TextView) findViewById(R.id.gps_distance_val);

        mButtonStartStop = (FancyButton) findViewById(R.id.start_stop);
        mButtonStartStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (toggle_pressed_state) {
                    stopService();
                } else {
                    startService();
                }
            }
        });
        mBbuttonOpenBT = (FancyButton) findViewById(R.id.open_bt);
        mBbuttonOpenBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentOpenBluetoothSettings = new Intent();
                intentOpenBluetoothSettings.setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
                startActivity(intentOpenBluetoothSettings);
            }
        });
        mButtonReset = (FancyButton) findViewById(R.id.id_reset);
        mButtonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResetButton();
            }
        });
        mButtonBTDiscovery = (FancyButton) findViewById(R.id.id_bt_discover);
        mButtonBTDiscovery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBtDiscoverableButton();
            }
        });

        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        mainActivityReference = this;
    }


    private void firstTimeApp() {

        SharedPreferences runCheck = getSharedPreferences("hasRunBefore", MODE_PRIVATE); //load the preferences
        Boolean hasRun = runCheck.getBoolean("hasRun", false); //see if it's run before, default no
        if (!hasRun) {
            //first time the app has run
            SharedPreferences settings = getSharedPreferences("hasRunBefore", MODE_PRIVATE);
            SharedPreferences.Editor edit = settings.edit();

            edit.putInt(getString(R.string.timeout), DEFAULT_TIMEOUT);
            edit.putInt(getString(R.string.timeout_count), DEFAULT_TIMEOUT_COUNT);
            edit.putInt(getString(R.string.gps_distance), DEFAULT_GPS_DISTANCE);
            edit.putInt(getString(R.string.gps_time), DEFAULT_GPS_TIME);
            edit.putBoolean("hasRun", true);        //set to has run
            edit.commit();                          //apply
        }
    }

    private void setSeekBar() {

        SharedPreferences settings = getSharedPreferences("hasRunBefore", MODE_PRIVATE); //load the preferences
        int timeout = settings.getInt(getString(R.string.timeout), DEFAULT_TIMEOUT);
        L.msg(TAG, "setting timeout to  " + timeout);
        mTimeout.setProgress(timeout);
        mTimeoutVal.setText(String.valueOf(timeout));

        int timeout_count = settings.getInt(getString(R.string.timeout_count), DEFAULT_TIMEOUT_COUNT);
        L.msg(TAG, "setting timeout count to  " + timeout_count);
        mTimeoutCount.setProgress(timeout_count);
        mTimeoutCountVal.setText(String.valueOf(timeout_count));

        int gps_distance = settings.getInt(getString(R.string.gps_distance), DEFAULT_GPS_DISTANCE);
        L.msg(TAG, "setting gps distance to  " + gps_distance);
        mGpsDistance.setProgress(gps_distance);
        mGpsDistanceVal.setText(String.valueOf(gps_distance));

        int gps_time = settings.getInt(getString(R.string.gps_time), DEFAULT_GPS_TIME);
        L.msg(TAG, "setting gps time to  " + gps_time);
        mGpsTime.setProgress(gps_time);
        mGpsTimeVal.setText(String.valueOf(gps_time));

        mTimeout.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                // TODO Auto-generated method stub
                if (progress == 0) {
                    progress = 1;
                    mTimeout.setProgress(progress);
                }
                mTimeoutVal.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }
        });
        mTimeoutCount.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                // TODO Auto-generated method stub
                if (progress < MIN_timeoutCount) {
                    progress = MIN_timeoutCount;
                    mTimeoutCount.setProgress(MIN_timeoutCount);
                }
                mTimeoutCountVal.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }
        });
        mGpsDistance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                // TODO Auto-generated method stub
                if (progress < MIN_gpsDistance) {
                    progress = MIN_gpsDistance;
                    mGpsDistance.setProgress(progress);
                }
                mGpsDistanceVal.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }
        });
        mGpsTime.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                // TODO Auto-generated method stub
                if (progress < MIN_gpsTime) {
                    progress = MIN_gpsTime;
                    mGpsTime.setProgress(progress);
                }
                mGpsTimeVal.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }
        });

    }

    private void updateUiStatus(int loopsNumber, int connectedNumber, int timedOutNumber, Boolean SIGFOX_client_connected,
                                int SIGFOX_sent_number) {

        String text = String.format(getResources().getString(R.string.status_main_activity), loopsNumber, timedOutNumber, connectedNumber,
                SIGFOX_client_connected, SIGFOX_sent_number);
        mStatusText.setText(text);
    }

    public void sendMessageToService(int messageType, String str, Integer val) {
        if (mIsBound) {
            if (mService != null) {
                try {
                    Bundle b = new Bundle();
                    if (str != null) b.putString("str1", str);
                    if (val != null) b.putInt("val1", val);
                    Message msg = Message.obtain(null, messageType);
                    msg.setData(b);
                    mService.send(msg);
                } catch (RemoteException e) {
                    L.msg(TAG, "sendMessageToService : FAILED");
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setSeekBar();
        IntentFilter intentFilter = new IntentFilter(RECEIVE_BROADCAST);
        registerReceiver(myReceiver, intentFilter);
        if (isMyServiceRunning(SigfoxBackgroundService.class) && (!mBtAdapter.isEnabled())) {
            stopService();
        }

        if (!checkBTnameValidity(mBtAdapter)) {
            if (isMyServiceRunning(SigfoxBackgroundService.class)) {
                stopService(new Intent(MainActivity.this, SigfoxBackgroundService.class));
            }
            mStatusText.setTextColor(Color.RED);
            String text = String.format(getResources().getString(R.string.BT_name_not_supported), mBtAdapter.getName());
            mStatusText.setTypeface(null, Typeface.BOLD);
            mStatusText.setText(text);

            mButtonStartStop.setVisibility(View.INVISIBLE);
            mButtonReset.setVisibility(View.INVISIBLE);
            mButtonBTDiscovery.setVisibility(View.INVISIBLE);

            mTimeout.setEnabled(false);
            mTimeoutCount.setEnabled(false);
            mGpsDistance.setEnabled(false);
            mGpsTime.setEnabled(false);

            return;
        }
        mStatusText.setTextColor(Color.GREEN);
        mStatusText.setTypeface(null, Typeface.NORMAL);
        mButtonStartStop.setVisibility(View.VISIBLE);
        mButtonReset.setVisibility(View.VISIBLE);
        mButtonBTDiscovery.setVisibility(View.VISIBLE);

        if (isMyServiceRunning(SigfoxBackgroundService.class)) {
            setButtonStopBackground();
            mTimeout.setEnabled(false);
            mTimeoutCount.setEnabled(false);
            mGpsDistance.setEnabled(false);
            mGpsTime.setEnabled(false);

            String text = String.format(getResources().getString(R.string.status_main_activity), Globals.loopsNumber, Globals.timedOutNumber,
                    Globals.connectedNumber, Globals.SIGFOX_client_connected, Globals.SIGFOX_sent_number);
            mStatusText.setText(text);
        } else {
            setButtonStartBackground();
            mTimeout.setEnabled(true);
            mTimeoutCount.setEnabled(true);
            mGpsDistance.setEnabled(true);
            mGpsTime.setEnabled(true);
            mStatusText.setText(getResources().getString(R.string.service_stopped));
        }
        if (!mIsBound)
            doBindService();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(myReceiver);
        doUnbindService();

        //saving seekbar values
        int timeout = mTimeout.getProgress();
        int timeout_count = mTimeoutCount.getProgress();
        int gps_distance = mGpsDistance.getProgress();
        int gps_time = mGpsTime.getProgress();

        SharedPreferences settings = getSharedPreferences("hasRunBefore", MODE_PRIVATE);
        SharedPreferences.Editor edit = settings.edit();

        edit.putInt(getString(R.string.timeout), timeout);
        edit.putInt(getString(R.string.timeout_count), timeout_count);
        edit.putInt(getString(R.string.gps_distance), gps_distance);
        edit.putInt(getString(R.string.gps_time), gps_time);
        edit.commit();
    }

    void doBindService() {
        bindService(new Intent(this, SigfoxBackgroundService.class), mCon, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    void doUnbindService() {
        if (mIsBound) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null, SigfoxBackgroundService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
            // Detach our existing connection.
            unbindService(mCon);
            mIsBound = false;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == REQUEST_ENABLE_BT) && (resultCode == RESULT_OK)) {
            boolean isEnabling = mBtAdapter.enable();
            if (!isEnabling) {
                // an immediate error occurred - perhaps the bluetooth is already on?
                L.msg(TAG, "onActivityResult:  perhaps the bluetooth is already on?");
            } else if (mBtAdapter.getState() == BluetoothAdapter.STATE_ON || mBtAdapter.getState()
                    == BluetoothAdapter.STATE_TURNING_ON) {
                startService(new Intent(MainActivity.this, SigfoxBackgroundService.class));
                /*if(!mIsBound)
                    doBindService();*/
                setButtonStartBackground();
                //disable sliders
                mTimeout.setEnabled(false);
                mTimeoutCount.setEnabled(false);
                mGpsDistance.setEnabled(false);
                mGpsTime.setEnabled(false);

                String text = String.format(getResources().getString(R.string.status_main_activity), Globals.loopsNumber, Globals.timedOutNumber,
                        Globals.connectedNumber, Globals.SIGFOX_client_connected, Globals.SIGFOX_sent_number);
                mStatusText.setText(text);
            }
        }
    }

    void setButtonStartBackground() {
        mButtonStartStop.setBackgroundColor(ContextCompat.getColor(this, R.color.start_button_color));
        mButtonStartStop.setFocusBackgroundColor(ContextCompat.getColor(this, R.color.stop_button_color));
        mButtonStartStop.setTextColor(ContextCompat.getColor(this, R.color.start_button_text_color));
        mButtonStartStop.setText(getResources().getString(R.string.start_button_text));
        toggle_pressed_state = false;
    }

    void setButtonStopBackground() {
        mButtonStartStop.setBackgroundColor(ContextCompat.getColor(this, R.color.stop_button_color));
        mButtonStartStop.setFocusBackgroundColor(ContextCompat.getColor(this, R.color.start_button_color));
        mButtonStartStop.setText(getResources().getString(R.string.stop_button_text));
        mButtonStartStop.setTextColor(ContextCompat.getColor(this, R.color.stop_button_text_color));
        toggle_pressed_state = true;
    }

    public void startService() {
        if (!mIsBound)
            doBindService();

        setButtonStopBackground();
        //always reset all counters and data before starting the service
        Globals.loopsNumber = 0;
        Globals.connectedNumber = 0;
        Globals.timedOutNumber = 0;
        Globals.SIGFOX_sent_number = 0;
        Globals.SIGFOX_client_connected = false;

        int timeout = Integer.parseInt(mTimeoutVal.getText().toString());
        int timeout_count = Integer.parseInt(mTimeoutCountVal.getText().toString());
        int gps_time = Integer.parseInt(mGpsTimeVal.getText().toString());
        int gps_distance = Integer.parseInt(mGpsDistanceVal.getText().toString());

        sendMessageToService(SigfoxBackgroundService.MSG_STRING_INT_VALUE, SigfoxBackgroundService.TIMEOUT_VALUE, timeout);
        sendMessageToService(SigfoxBackgroundService.MSG_STRING_INT_VALUE, SigfoxBackgroundService.TIMEOUT_COUNT, timeout_count);
        sendMessageToService(SigfoxBackgroundService.MSG_STRING_INT_VALUE, SigfoxBackgroundService.GPS_AUTOUPDATE_DST, gps_distance);
        sendMessageToService(SigfoxBackgroundService.MSG_STRING_INT_VALUE, SigfoxBackgroundService.GPS_AUTOUPDATE_TIME, gps_time);

        // Getting GPS status
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // Getting network status
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (!isGPSEnabled && !isNetworkEnabled) {
            Toast.makeText(getBaseContext(), "GPS is OFF. Please set GPS ON", Toast.LENGTH_LONG).show();
            startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            return;
        }
        if (!mBtAdapter.isEnabled()) {
            // prompt the user to turn BlueTooth on
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        } else {
            startService(new Intent(MainActivity.this, SigfoxBackgroundService.class));
            setButtonStopBackground();
            mTimeout.setEnabled(false);
            mTimeoutCount.setEnabled(false);
            mGpsDistance.setEnabled(false);
            mGpsTime.setEnabled(false);
            mStatusText.setText(getResources().getString(R.string.service_started));
            String text = String.format(getResources().getString(R.string.status_main_activity), Globals.loopsNumber, Globals.timedOutNumber,
                    Globals.connectedNumber, Globals.SIGFOX_client_connected, Globals.SIGFOX_sent_number);
            mStatusText.setText(text);
        }
    }

    public void onResetButton() {
        Globals.loopsNumber = 0;
        Globals.connectedNumber = 0;
        Globals.timedOutNumber = 0;
        Globals.SIGFOX_sent_number = 0;
        Globals.SIGFOX_client_connected = false;
        String text = String.format(getResources().getString(R.string.status_main_activity), Globals.loopsNumber, Globals.timedOutNumber,
                Globals.connectedNumber, Globals.SIGFOX_client_connected, Globals.SIGFOX_sent_number);
        mStatusText.setText(text);
    }

    public void stopService() {
        doUnbindService();
        stopService(new Intent(MainActivity.this, SigfoxBackgroundService.class));
        setButtonStartBackground();
        //enable slider
        mTimeout.setEnabled(true);
        mTimeoutCount.setEnabled(true);
        mGpsDistance.setEnabled(true);
        mGpsTime.setEnabled(true);
        mStatusText.setText(getResources().getString(R.string.service_stopped));
    }

    public void onBtDiscoverableButton() {
        if (mBtAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
            mStatusText.setText(getResources().getString(R.string.asked_discoverable));
            L.msg(TAG, "Bluetooth device desirability asked for 5 minutes");
        } else {
            mStatusText.setText(getResources().getString(R.string.already_discoverable));
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private Boolean checkBTnameValidity(BluetoothAdapter mBtAdapter) {
        String[] strings;

        if (mBtAdapter.getName().contains("_") && mBtAdapter.getName().contains(keyNameTAG)) {
            strings = TextUtils.split(mBtAdapter.getName(), "_");
            if (!android.text.TextUtils.isDigitsOnly(strings[0])) {
                L.msg(TAG, "Name on BT device should start with: <number>_SIGFOX (ex: 1_SIGFOX). Now the name is: " + mBtAdapter.getName());
                L.msg(TAG, "Name does not start with a number. Sending over SIGFOX aborted. Rename  BT device ");
                return false;
            }
        } else {
            L.msg(TAG, "Name on BT dvice should start with: <number>_SIGFOX (ex: 1_SIGFOX). Now the name is: " + mBtAdapter.getName());
            L.msg(TAG, "Name does not contain '_' Sending over SIGFOX aborted. Rename  BT device ");
            return false;
        }
        return true;
    }

    static class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case SigfoxBackgroundService.MSG_STRING_INT_VALUE:
                    String str1 = msg.getData().getString("str1");
                    Integer val1 = msg.getData().getInt("val1");
                    Integer val2 = msg.getData().getInt("val2");
                    Integer val3 = msg.getData().getInt("val3");
                    Boolean val4 = msg.getData().getBoolean("val4");
                    Integer val5 = msg.getData().getInt("val5");
                    if (str1 != null) {
                        if (str1.equals(SigfoxBackgroundService.SEND_STATUS))
                            MainActivity.mainActivityReference.updateUiStatus(val1, val2, val3, val4, val5);
                    }
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }
}
