package com.intel.sigfoxclientapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.text.TextUtils;

import com.intel.telephony.lpwan.ILpwanClient;
import com.intel.telephony.lpwan.LpwanClientConnection;
import com.intel.telephony.lpwan.LpwanClientException;
import com.intel.telephony.lpwan.LpwanListener;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import ch.hsr.geohash.GeoHash;
import ch.hsr.geohash.WGS84Point;

/**
 * Service which will run in background and listen for  BT incomming connnection and signal using
 * SIGFOX network when the case
 */
public class SigfoxBackgroundService extends Service {
    public static final String SigfoxBackgroundSVC = "com.intel.SigfoxBackgroundService";
    static final int MSG_REGISTER_CLIENT = 1;
    static final int MSG_UNREGISTER_CLIENT = 2;
    static final int MSG_STRING_INT_VALUE = 3;
    static final int MSG_STATUS_REQUEST = 4;
    static final String SOCKET_ACCEPT_TIMED_OUT = "SOCKET_ACCEPT_TIMED_OUT";
    static final String SOCKET_ACCEPT_OK = "SOCKET_ACCEPT_OK";
    static final String BT_ADAPTER_TURNED_OFF = "BT_ADAPTER_TURNED_OFF";
    static final String TIMEOUT_VALUE = "TIMEOUT_VALUE";
    static final String TIMEOUT_COUNT = "TIMEOUT_COUNT";
    static final String GPS_AUTOUPDATE_DST = "GPS_AUTOUPDATE_DST";
    static final String GPS_AUTOUPDATE_TIME = "GPS_AUTOUPDATE_TIME";
    static final String SEND_STATUS = "SEND_STATUS";
    private static final String TAG = "SgfClientService";
    static private final Boolean LOGS = true, TOASTS = true;
    private final static int geoHashPrecisionCharacterNumber = 9; //precision of GPS location on geoHash method
    private final static int EVENT_CLIENT_CONNECTED = 1;
    private final static int EVENT_CLIENT_DISCONNECTED = 2;
    private final static int EVENT_DATA_SENT = 3;
    public static BluetoothAdapter mBtAdapter;
    private static int incomingBtTimeoutAccept = 10000;  //ms
    private static int timeoutCount = 4;
    private static long gpsAutoUpdateDistanceVal = 0, gpsAutoUpdateTimeVal = 1000 * 60 * 5;
    private static boolean isRunning = false, gpsAutoUpdateOn = false;
    final Messenger mMessenger = new Messenger(new IncomingClientHandler(this)); // Target we publish for clients to send messages to IncomingHandler.
    final Handler mHandler = new Handler(new IncomingSIGFOXHandlerCallback());
    //listener for incoming SIGFOX events
    private final LpwanListener mLpwanListener = new LpwanListener() {
        @Override
        public void onDataSent(int requestId, byte[] downlinkData) {
            mHandler.obtainMessage(EVENT_DATA_SENT).sendToTarget();
        }

        @Override
        public void onModemVersionRead(int requestId, String modemVersion) {

        }

        @Override
        public void onDeviceIdRead(int requestId, String deviceId) {

        }

        @Override
        public void onModemReset(int requestId) {

        }

        @Override
        public void onFailure(int requestId, int reason) {

        }
    };
    protected BtConnectionWorker thread;
    protected Handler handler;
    GPSTracker gps = null;
    ArrayList<Messenger> mClients = new ArrayList<>(); // Keeps track of all current registered clients.
    private NotificationManager nm;
    private ILpwanClient mLpwanClient = null;
    private LpwanClientConnection mLpwanConnection = null;
    private int mLpwanServiceId = -1;

    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    private void sendStringMessageToUI(int messageType, String str, Integer loopsNumber, Integer connectedNumber,
                                       Integer timedOutNumber, Boolean sigfoxClientConnected, Integer sigfoxSendMsgNumber) {
        for (int i = mClients.size() - 1; i >= 0; i--) {
            try {
                Bundle b = new Bundle();
                b.putString("str1", str);
                b.putInt("val1", loopsNumber);
                b.putInt("val2", connectedNumber);
                b.putInt("val3", timedOutNumber);
                b.putBoolean("val4", sigfoxClientConnected);
                b.putInt("val5", sigfoxSendMsgNumber);

                Message msg = Message.obtain(null, messageType);
                msg.setData(b);
                mClients.get(i).send(msg);
                if (LOGS) L.msg(TAG, "msg size TX to UI: " + b.size());
            } catch (RemoteException e) {
                // The client is dead. Remove it from the list; we are going through the list from back to front so this is safe to do inside the loop.
                mClients.remove(i);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stopping the thread.
        if (isRunning) {
            if (this.thread.isAlive()) {
                this.thread.running = false;
                this.thread.interrupt();
            }
            if (nm != null) nm.cancelAll();
            if (gps != null) {
                gps.stopGPSautoUpdates();
                gpsAutoUpdateOn = false;
                L.msg(TAG, "GPS / NETWORK Location Auto Updates stopped. Service destroyed");
            }
            if (mLpwanConnection != null)
                mLpwanConnection.disconnect();
            if (LOGS) L.msg(TAG, "Service Stopped.");
            isRunning = false;
            L.toast(getBaseContext(), "SigfoxBackgroundService stopped");
        }

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        gps = new GPSTracker(getApplicationContext(), this);

        if (!mBtAdapter.isEnabled()) {
            if (LOGS) L.msg(TAG, "Bluetooth is OFF. Cannot start service.");
            onDestroy();
        } else {
            if (LOGS) L.msg(TAG, "Service Started.");
        }

        if (LOGS) L.msg(TAG, "Received start id " + startId + ": " + intent);
        nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (isRunning) {
            return Service.START_STICKY;
        } else {
            isRunning = true;
        }
        Notification notification = new Notification();
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
            notification.priority = Notification.PRIORITY_MIN;
        }
        startForeground(R.string.app_name, notification);

        Globals.SIGFOX_client_connected = false;
        mLpwanConnection = new LpwanClientConnection(this,
                new LpwanClientConnection.ILpwanClientConnectionListener() {
                    @Override
                    public void onClientConnected(ILpwanClient client) {
                        mLpwanClient = client;
                        try {
                            mLpwanServiceId = mLpwanClient.open(mLpwanListener);
                        } catch (LpwanClientException e) {
                            if (LOGS) L.msg(TAG, "SIGFOX modem msg: " + e.toString());
                        }
                        mHandler.obtainMessage(EVENT_CLIENT_CONNECTED).sendToTarget();
                    }

                    @Override
                    public void onClientDisconnected() {
                        mLpwanClient = null;
                        mHandler.obtainMessage(EVENT_CLIENT_DISCONNECTED).sendToTarget();
                    }
                });
        try {
            mLpwanConnection.connect();
        } catch (LpwanClientException e) {
            if (LOGS) L.msg(TAG, "SIGFOX modem msg: " + e.toString());
        }
        // Starting the worker thread.
        startConnectionWorkerThread();

        return Service.START_STICKY; // run until explicitly stopped.
    }

    private void btSocketTimedOut() {
        if (Globals.timedOutNumber % timeoutCount == 0)
            if (!gpsAutoUpdateOn)
                SignalClientLost();
    }

    private void btSocketConnected() {
        Globals.timedOutNumber = 0;
        //if we connect to master while lost, then we declare it found and stop autoUpdate of GPS
        if (gpsAutoUpdateOn) {
            gps.stopGPSautoUpdates();
            gpsAutoUpdateOn = false;
            if (LOGS) L.msg(TAG, "GPS / NETWORK Location Auto Updates stopped");
        }
    }

    private void SignalClientLost() {
        if (!gpsAutoUpdateOn) {
            gps.requestLocationUpdatesOverGPS(gpsAutoUpdateTimeVal * 1000, gpsAutoUpdateDistanceVal);
            gps.requestLocationUpdatesOverNetwork(gpsAutoUpdateTimeVal * 1000, gpsAutoUpdateDistanceVal);
            gpsAutoUpdateOn = true;
            if (LOGS)
                L.msg(TAG, "GPS / NETWORK Location Auto Updates started. AutoUpdate TIMER interval: " + gpsAutoUpdateTimeVal
                        + " sec, min DISTANCE : " + gpsAutoUpdateDistanceVal + " meters."
                        + " GPS hw update location interval: " + gpsAutoUpdateTimeVal / GPSTracker.gpsRefreshRate + " sec");
        }
    }

    //method called from GPSTracker, whenever location INFO update is available
    public void LocationUpdateCallback(Location location) {
        double gpsCoordinates[] = {location.getLatitude(), location.getLongitude(), location.getAccuracy()};

        if (LOGS)
            L.msg(TAG, "GPS coordinates to send " + gpsCoordinates[0] + " lat " + gpsCoordinates[1] + " long. Location AutoUpdate"
                    + " Accuracy: " + gpsCoordinates[2] + " m");
        if (mLpwanClient != null) {
            if (LOGS) L.msg(TAG, "Try to send over SIGFOX !");
            L.toast(getBaseContext(), "Your Location is - \nLat: " + gpsCoordinates[0] + "\nLong: " + gpsCoordinates[1] +
                    "\nAccuracy: " + gpsCoordinates[2] + " m" + "\nLocation Autoupdate ");
            sendDataUsingSigfoxNetwork(gpsCoordinates);
        } else {
            if (TOASTS) {
                L.toast(getBaseContext(), "Your Location is - \nLat: " + gpsCoordinates[0] + "\nLong: " + gpsCoordinates[1] +
                        "\nAccuracy: " + gpsCoordinates[2] + " m" + "\nLocation Autoupdate ");
            }
        }
    }

    private void sendDataUsingSigfoxNetwork(double[] coordinates) {
        double latitude = coordinates[0];
        double longitude = coordinates[1];
        String joined;
        String[] concatenate, strings;

        //encode coordinates using GeoHash
        GeoHash mGeoHash = EncodingWithCharacterPrecision(new WGS84Point(latitude, longitude), geoHashPrecisionCharacterNumber);
        if (LOGS) L.msg(TAG, "geoHash name generated : " + mGeoHash.toBase32());
        //check to see if BT  name corresponds to the template: <number>SIGFOX<user defined if any>
        strings = TextUtils.split(mBtAdapter.getName(), "_");
        if (mBtAdapter.getName().contains("_")) {
            if (!android.text.TextUtils.isDigitsOnly(strings[0])) {
                if (LOGS)
                    L.msg(TAG, "Name on BT device should start with: <number>_SIGFOX (ex: 1_SIGFOX). Now the name is: " + mBtAdapter.getName());
                if (LOGS)
                    L.msg(TAG, "Name does not start with a number. Sending over SIGFOX aborvamos a ver cuanto duro" +
                            "x ahora me gusta\t" +
                            "ed. Rename  BT device ");
                return;
            }
        } else {
            if (LOGS)
                L.msg(TAG, "Name on BT device should start with: <number>_SIGFOX (ex: 1_SIGFOX). Now the name is: " + mBtAdapter.getName());
            if (LOGS)
                L.msg(TAG, "Name does not contain '_' Sending over SIGFOX aborted. Rename  BT device ");
            return;
        }
        //put 2 strings in array
        concatenate = new String[]{strings[0], mGeoHash.toBase32()};
        //concatenate with an delimitator
        joined = TextUtils.join(";", concatenate);
        //convert new joined string to bytes array to be sent to SIGFOX network
        byte[] byteArray = joined.getBytes();
        if (LOGS) L.msg(TAG, "Sending payload over SIGFOX: " + joined);
        try {
            L.toast(getBaseContext(), "Sending message over SIGFOX network...");
            mLpwanClient.sendData(mLpwanServiceId, byteArray, false);
        } catch (LpwanClientException e) {
            if (LOGS) L.msg(TAG, "SIGFOX modem msg" + e.toString());
        }

        //For split and decode use below code
        //strings = TextUtils.split(joined, ";");

        //decode GeoHash data
           /* GeoHash decodedHash = GeoHash.fromGeohashString(mGeoHash.toBase32());
            WGS84Point decodedCenter = decodedHash.getBoundingBoxCenterPoint();
            L.toast(getBaseContext(), "Your Location is - \nLat: " + decodedCenter.getLatitude() + "\nLong: " + decodedCenter.getLongitude() + "\nDECODED "); */
    }

    private GeoHash EncodingWithCharacterPrecision(WGS84Point point, int numberOfCharacters) {
        return (GeoHash.withCharacterPrecision(point.getLatitude(), point.getLongitude(), numberOfCharacters));
    }

    private void startConnectionWorkerThread() {
        if (isRunning) {
            this.handler = new LocalHandler(this);
            this.thread = new BtConnectionWorker(this.handler, incomingBtTimeoutAccept);
            if (LOGS) L.msg(TAG, "Starting working thread " + incomingBtTimeoutAccept + " ms");
            // Starting the worker thread.
            this.thread.start();
        }
    }

    //incoming messages from worker Thread
    static class LocalHandler extends Handler {
        final SigfoxBackgroundService sigfoxBackgroundService;

        public LocalHandler(SigfoxBackgroundService sigfoxBackgroundService) {
            this.sigfoxBackgroundService = sigfoxBackgroundService;
        }

        @Override
        public void handleMessage(Message msg) {
            String str1 = msg.getData().getString("str1");

            if (LOGS) L.msg(TAG, "Message received from worker thread: " + str1);
            if (str1 != null) {
                if (str1.equals(SOCKET_ACCEPT_TIMED_OUT)) {
                    sigfoxBackgroundService.btSocketTimedOut();
                }
                if (str1.equals(SOCKET_ACCEPT_OK)) {
                    sigfoxBackgroundService.btSocketConnected();
                }
                if (str1.equals(BT_ADAPTER_TURNED_OFF)) {
                    Intent i = new Intent(MainActivity.RECEIVE_BROADCAST);
                    Bundle extras = new Bundle();
                    extras.putString("command", "stop");
                    i.putExtras(extras);
                    sigfoxBackgroundService.sendBroadcast(i);
                }
            }
            sigfoxBackgroundService.sendStringMessageToUI(MSG_STRING_INT_VALUE, SEND_STATUS, Globals.loopsNumber,
                    Globals.connectedNumber, Globals.timedOutNumber, Globals.SIGFOX_client_connected, Globals.SIGFOX_sent_number);
        }
    }

    // Handler of incoming messages from clients.
    static class IncomingClientHandler extends Handler {
        private final WeakReference<SigfoxBackgroundService> mService;

        IncomingClientHandler(SigfoxBackgroundService service) {
            mService = new WeakReference<>(service);
        }

        @Override
        public void handleMessage(Message msg) {
            SigfoxBackgroundService service = mService.get();
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    service.mClients.add(msg.replyTo);
                    if (LOGS) L.msg(TAG, "Clients REGISTERED: " + service.mClients.size());
                    break;
                case MSG_UNREGISTER_CLIENT:
                    service.mClients.remove(msg.replyTo);
                    if (LOGS) L.msg(TAG, "Client UNREGISTERED from service");
                    break;
                case MSG_STRING_INT_VALUE:
                    String str1 = msg.getData().getString("str1");
                    Integer val1 = msg.getData().getInt("val1");

                    if (str1 != null) {
                        if (str1.equals(TIMEOUT_VALUE)) {
                            incomingBtTimeoutAccept = val1 * 1000;
                        }
                        if (str1.equals(TIMEOUT_COUNT)) {
                            timeoutCount = val1;
                        }
                        if (str1.equals(GPS_AUTOUPDATE_DST)) {
                            gpsAutoUpdateDistanceVal = val1;
                        }
                        if (str1.equals(GPS_AUTOUPDATE_TIME)) {
                            gpsAutoUpdateTimeVal = val1;
                        }
                    }
                    break;

                case MSG_STATUS_REQUEST:
                    service.sendStringMessageToUI(MSG_STRING_INT_VALUE, SEND_STATUS, Globals.loopsNumber,
                            Globals.connectedNumber, Globals.timedOutNumber, Globals.SIGFOX_client_connected, Globals.SIGFOX_sent_number);
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    }

    //Handler callback for SIGFOX stack
    class IncomingSIGFOXHandlerCallback implements Handler.Callback {
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                sendStringMessageToUI(MSG_STRING_INT_VALUE, SEND_STATUS, Globals.loopsNumber,
                        Globals.connectedNumber, Globals.timedOutNumber, Globals.SIGFOX_client_connected, Globals.SIGFOX_sent_number);
            }
        };

        @Override
        public boolean handleMessage(Message msg) {

            switch (msg.what) {
                case EVENT_CLIENT_CONNECTED:
                    //  Toast.makeText(getBaseContext(), "LPWAN client connected", Toast.LENGTH_LONG).show();
                    if (LOGS) L.msg(TAG, "LPWAN SIGFOX client connected");
                    Globals.SIGFOX_client_connected = true;
                    break;
                case EVENT_CLIENT_DISCONNECTED:
                    // Toast.makeText(getBaseContext(), "LPWAN client disconnected", Toast.LENGTH_LONG).show();
                    if (LOGS) L.msg(TAG, "LPWAN SIGFOX client disconnected");
                    Globals.SIGFOX_client_connected = false;
                    break;
                case EVENT_DATA_SENT:
                    //  Toast.makeText(getBaseContext(), "Data sent", Toast.LENGTH_LONG).show();
                    if (LOGS) L.msg(TAG, "LPWAN SIGFOX data sent !");
                    Globals.SIGFOX_sent_number++;
                    break;
            }
            mHandler.post(myRunnable);
            return true;
        }
    }
}
