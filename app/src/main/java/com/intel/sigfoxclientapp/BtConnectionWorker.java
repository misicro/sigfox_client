package com.intel.sigfoxclientapp;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public class BtConnectionWorker extends Thread {
    private static final String TAG = "BtConnectionWorkerThread";
    private final Boolean LOGS = true;
    // Reference to service's handler.
    protected Handler handler;
    BluetoothAdapter bluetoothAdapter;
    BluetoothSocket bluetoothSocket = null;
    InputStream mBTInputStream;
    OutputStream mBTOutputStream;
    volatile boolean running = true;
    Boolean timeOutVar = false;
    private BluetoothServerSocket bluetoothServerSocket = null;
    private int acceptTimeout = 0;

    public BtConnectionWorker(Handler handler, int timeout) {
        super(BtConnectionWorker.class.getName());
        resetConnection();
        UUID myUUID = UUID.fromString("ec79da00-853f-11e4-b4a9-0800200c9a66");
        String myName = myUUID.toString();
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // Saving injected reference.
        this.handler = handler;
        this.acceptTimeout = timeout;

        if (bluetoothServerSocket != null) {
            try {
                bluetoothServerSocket.close();
            } catch (Exception e) {
                if (LOGS) L.msg(TAG, "resetConnection " + e);
            }
            bluetoothServerSocket = null;
        }
        if (LOGS) L.msg(TAG, "Trying to open bluetoothServerSocket");
        try {
            bluetoothServerSocket = bluetoothAdapter.listenUsingRfcommWithServiceRecord(myName, myUUID);
        } catch (IOException e) {
            resetConnection();
            if (LOGS) L.msg(TAG, "FAIL to get SOCKET :" + bluetoothServerSocket);
        }
    }

    @Override
    public void run() {
        try {
            while(running) {
                if (this.isInterrupted()) {
                    // Terminating this method when thread is interrupted.
                    resetConnection();
                    return;
                }
                if (!bluetoothAdapter.isEnabled()) {
                    // Terminating this method when thread is interrupted.
                    resetConnection();
                    sendMessageToService(SigfoxBackgroundService.BT_ADAPTER_TURNED_OFF, 0);
                    if (LOGS) L.msg(TAG, "Bluetooth adapter was turned OFF. Shutting down service.");
                    return;
                }
                if (bluetoothServerSocket != null) {
                    try {
                        bluetoothSocket = bluetoothServerSocket.accept(acceptTimeout);
                        timeOutVar = false;
                    } catch (IOException e) {
                        if (LOGS)
                           if (LOGS) L.msg(TAG, "accept timeout. " + Integer.toString(acceptTimeout / 1000) + " seconds passed");
                        Globals.timedOutNumber++;
                        sendMessageToService(SigfoxBackgroundService.SOCKET_ACCEPT_TIMED_OUT, 0);
                        timeOutVar = true;
                    }
                    if(!timeOutVar ) {
                        Globals.connectedNumber++;
                        if (LOGS) L.msg(TAG, "socket accept OK. Successful attempts: " + Globals.connectedNumber);
                        resetConnection();
                        sendMessageToService(SigfoxBackgroundService.SOCKET_ACCEPT_OK, 0);
                    }
                    Globals.loopsNumber++;
                } else {
                    if (LOGS) L.msg(TAG, "Bluetooth SOCKET null returned");
                    resetConnection();
                }
            }
            if(!running) {
                //close listening server socket
                if (bluetoothServerSocket != null) {
                    try {
                        bluetoothServerSocket.close();
                    } catch (Exception e) {
                       if (LOGS) L.msg(TAG, "resetConnection: " + e);
                    }
                    bluetoothServerSocket = null;
                }
                //close bluetooth socket and IN/OUT streams, if active
                resetConnection();
                bluetoothAdapter = null;
            }
        } catch (Exception e) {
      //      this.e = e;
           if (LOGS) L.msg(TAG, "workerThreadIssue: " + e);
        }
    }

    //close connection, in order to move to next one
    private void resetConnection() {
        if (mBTInputStream != null) {
            try {
                mBTInputStream.close();
            } catch (Exception e) {
               if (LOGS) L.msg(TAG, "resetConnection " + e);
            }
            mBTInputStream = null;
        }
        if (mBTOutputStream != null) {
            try {
                mBTOutputStream.close();
            } catch (Exception e) {
               if (LOGS) L.msg(TAG, "resetConnection " + e);
            }
            mBTOutputStream = null;
        }
        if (bluetoothSocket != null) {
            try {
                bluetoothSocket.close();
            } catch (Exception e) {
               if (LOGS) L.msg(TAG, "resetConnection " + e);
            }
            bluetoothSocket = null;
        }
    }

    private void sendMessageToService(String str, Integer  val) {

        // Sending a message back to the service via handler.
        Bundle b = new Bundle();
        b.putString("str1", str);
        b.putInt("val1",  val);
        Message message = this.handler.obtainMessage();
        message.setData(b);
        this.handler.sendMessage(message);
    }
}
