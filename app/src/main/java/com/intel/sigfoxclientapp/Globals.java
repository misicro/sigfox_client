package com.intel.sigfoxclientapp;
/**
 * Store globals
 */
public class Globals {
    public static int loopsNumber;
    public static int connectedNumber;
    public static int timedOutNumber;
    public static int SIGFOX_sent_number;
    public static Boolean SIGFOX_client_connected;
}
